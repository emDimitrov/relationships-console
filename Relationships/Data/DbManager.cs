﻿using Relationships.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Relationships.Data
{
    public abstract class DbManager
    {
        public static void LoadTestData(ref Student[] students,ref Friendship[] friendships)
        {
            students = new Student[6];
            friendships = new Friendship[7];

            Student student = new Student(1, "John", Gender.Male);
            Student student1 = new Student(2, "Peter", Gender.Male);
            Student student2 = new Student(3, "Carla", Gender.Female);
            Student student3 = new Student(4, "Jane", Gender.Female);
            Student student4 = new Student(5, "Igor", Gender.Male);
            Student student5 = new Student(6, "Petq", Gender.Female);
            students[0] = student;
            students[1] = student1;
            students[2] = student2;
            students[3] = student3;
            students[4] = student4;
            students[5] = student5;

            Friendship friendship = new Friendship(student, student1);
            Friendship friendship1 = new Friendship(student, student3);
            Friendship friendship2 = new Friendship(student2, student3);
            Friendship friendship3 = new Friendship(student1, student4);
            Friendship friendship4 = new Friendship(student1, student5);
            Friendship friendship5 = new Friendship(student3, student);
            Friendship friendship6 = new Friendship(student3, student1);
            friendships[0] = friendship;
            friendships[1] = friendship1;
            friendships[2] = friendship2;
            friendships[3] = friendship3;
            friendships[4] = friendship4;
            friendships[5] = friendship5;
            friendships[6] = friendship6;
        }
    }
}
