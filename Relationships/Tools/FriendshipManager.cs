﻿using Relationships.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Relationships.Tools
{
    public class FriendshipManager
    {
        public Friendship[] GetStudentFriendships(Friendship[] array, int value)
        {
            int low = 0, high = array.Length - 1, midpoint = 0;
            List<Friendship> friendships = new List<Friendship>();

            while (low <= high)
            {
                midpoint = low + (high - low) / 2;

                // check if value is equal to item in array
                if (value == array[midpoint].FirstStudent.FacultyNumber)
                {
                    friendships.Add(array[midpoint]);
                    int first = midpoint;
                    int last = midpoint;

                    // check if there are values to the left 
                    // also equal to the searched element
                    while (first > 0 && array[first - 1].FirstStudent.FacultyNumber == value)
                    {
                        friendships.Add(array[first - 1]);
                        first--;
                    }

                    // check if there are values to the right 
                    // also equal to the searched element
                    while (last < array.Length - 1 && array[last + 1].FirstStudent.FacultyNumber == value)
                    {
                        friendships.Add(array[last + 1]);
                        last++;
                    }
                    break;
                }
                else if (value < array[midpoint].FirstStudent.FacultyNumber)
                    high = midpoint - 1;
                else
                    low = midpoint + 1;
            }
            return friendships.ToArray();
        }

        public List<Student> GetUnknownPeople(List<Student> friends, List<Student> friendsPals)
        {
            List<Student> unknownPeople = new List<Student>();
            foreach (var student in friendsPals)
            {
                if (Array.BinarySearch(friends.ToArray(), student) < 0)
                {
                    unknownPeople.Add(student);
                }
            }

            return unknownPeople;
        }
    }
}
